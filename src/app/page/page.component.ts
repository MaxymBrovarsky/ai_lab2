import { Component, OnInit } from '@angular/core';
import {Plotly} from "angular-plotly.js/src/app/shared/plotly.interface";
import {PlotlyModule} from "angular-plotly.js";

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {

  speeds: number[] = [];
  currentInputSpeed = 30;
  expertCoefficient: number[] = [];
  pairComparisonMatrix: number[][] = [];
  expertBlock = false;
  comparisonBlock = false;
  graph = false;
  plotData = [
    {
      x: [],
      y: [],
    }

  ];
  plotLayout = {
    title: 'Графік невизначеної множини для швидкостей'
  };
  constructor() {

  }

  public addSpeed() {
    console.log(this.currentInputSpeed);
    this.speeds.push(this.currentInputSpeed);
  }

  public calculatePairComparisonMatrix() {
    this.expertBlock = true;

  }

  public getMaxSpeed(): number {
    return Math.max(...this.speeds);
  }

  public show(): void {
    const zipped = this.speeds.map((e, i) => {
      return [e, this.expertCoefficient[i]];
    });
    const m = this.pairComparisonMatrix;
    zipped.sort((a, b) => a[0] - b[0]);
    for (let i = 0; i < zipped.length; i++) {
      m[i] = [];

    }
    for (let j = 0; j < zipped.length; j++) {
      m[zipped.length - 1][j] = zipped[j][1];
    }
    for (let i = this.speeds.length - 2; i >= 0 ; i--) {
      for (let j = 0; j < this.expertCoefficient.length; j++) {
        this.calculateComparisonValue(i , j);
      }
    }
    m[this.speeds.length] = [];
    m[this.speeds.length + 1] = [];
    m[this.speeds.length + 2] = [];
    for (let i = 0 ; i < this.speeds.length; i++) {
      m[this.speeds.length][i] =  this.calculateSumOfColumn(i);
      m[this.speeds.length + 1][i] =  1 / m[this.speeds.length][i];
    }
    for (let i = 0 ; i < this.speeds.length; i++) {
      m[this.speeds.length + 2][i] =  m[this.speeds.length + 1][i] / Math.max(...m[this.speeds.length + 1]);
    }
    this.comparisonBlock = true;
  }

  private calculateSumOfColumn(index: number): number {
    let sum = 0;
    for (let i = 0; i < this.speeds.length; i++) {
      sum += this.pairComparisonMatrix[i][index];
    }
    return sum;
  }

  private calculateComparisonValue(i: number, j: number): void {
    const m = this.pairComparisonMatrix;
    const a = m[i + 1][j] / m[i + 1][i];
    this.pairComparisonMatrix[i][j] = a;
  }

  public setPlotData(): void {
    const line = {x: [], y: []};
    for (let i = 0; i < this.speeds.length; i++) {
      line.x.push(this.speeds[i]);
      line.y.push(this.pairComparisonMatrix[this.speeds.length + 2][i]);
    }
    this.plotData.push(line);
    this.graph = true;

  }

  public clearData() {
    this.plotData = [];
    this.speeds = [];
    this.pairComparisonMatrix = [];
    this.expertCoefficient = [];
    this.expertBlock = false;
    this.comparisonBlock = false;
    this.graph = false;
  }


  ngOnInit() {
  }

}
